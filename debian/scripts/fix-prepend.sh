#!/bin/sh
for i in $@
do
  ## replace all occurences of [prepend] by a subpatch using [list prepend]+[list trim]
  sed \
   -e 's|^#X obj \([0-9]* [0-9]*\) prepend \([^;]*\);|#N canvas 0 0 200 150 prepend 0; #X obj 0 20 inlet; #X obj 0 40 list prepend \2; #X obj 0 60 list trim; #X obj 0 80 outlet; #X connect 0 0 1 0; #X connect 1 0 2 0; #X connect 2 0 3 0; #X restore \1 pd prepend;|' \
   -i "${i}"
done
