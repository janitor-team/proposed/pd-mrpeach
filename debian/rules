#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2015-2016 IOhannes m zmölnig <umlaeute@debian.org>
# Description: Main Debian packaging script for mrpeach

pkglibdir = /usr/lib/pd/extra

#enable hardening
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

# get SOURCE_DATE_EPOCH and DEB_VERSION
include /usr/share/dpkg/pkg-info.mk

## set timestamp for reproducible builds
DATE_FMT = %Y/%m/%d at %H:%M:%S UTC
ifdef SOURCE_DATE_EPOCH
    BUILD_DATE ?= $(shell date -u -d "@$(SOURCE_DATE_EPOCH)" "+$(DATE_FMT)" 2>/dev/null || date -u -r "$(SOURCE_DATE_EPOCH)" "+$(DATE_FMT)" 2>/dev/null || date -u "+$(DATE_FMT)")
else
    BUILD_DATE ?= $(shell date "+$(DATE_FMT)")
endif
CPPFLAGS +=-DBUILD_DATE='\"compiled for Debian on $(BUILD_DATE)\"'

CFLAGS+=$(CPPFLAGS) -I/usr/include/pd

%:
	dh $@

override_dh_auto_clean \
override_dh_auto_build \
override_dh_auto_test:
	$(patsubst override_%,%,$@) -- \
		PDLIBBUILDER_DIR=/usr/share/pd-lib-builder/ \
                prefix=/usr pkglibdir=$(pkglibdir) \
                arch.flags="" \
                CPPFLAGS="$(CPPFLAGS)" \
                CFLAGS="$(CPPFLAGS) $(CFLAGS)" \
                LDFLAGS="$(LDFLAGS)" \
                $(empty)

override_dh_auto_install:
	dh_auto_install -- \
		PDLIBBUILDER_DIR=/usr/share/pd-lib-builder/ \
                prefix=/usr pkglibdir=$(pkglibdir) \
                arch.flags="" \
                CPPFLAGS="$(CPPFLAGS)" \
                CFLAGS="$(CPPFLAGS) $(CFLAGS)" \
                LDFLAGS="$(LDFLAGS)" \
                $(empty)
# fix permissions
	find $(CURDIR)/debian/tmp/ -name "*.pd_linux" -exec \
		chmod 0644 {} +
# replace license file with link to the Debian license file
	rm -f -- $(CURDIR)/debian/tmp/$(pkglibdir)/$(LIBRARY_NAME)/LICENSE.txt

override_dh_installchangelogs:
	dh_installchangelogs debian/upstream_changelog

override_dh_gencontrol:
	dh_gencontrol --package=pd-osc -- -v1:$(DEB_VERSION_UPSTREAM_REVISION)
	dh_gencontrol --remaining-packages
	for p in cmos mrpeach-net osc slip xbee; do\
		if [ -f debian/.debhelper/pd-$$p/dbgsym-root/DEBIAN/control ]; then\
			echo "Conflicts: pd-mrpeach-dbgsym" >> debian/.debhelper/pd-$$p/dbgsym-root/DEBIAN/control\
	; fi; done
	if [ -f debian/.debhelper/pd-mrpeach/dbgsym-root/DEBIAN/control ]; then\
		echo "Conflicts: pd-cmos-dbgsym, pd-mrpeach-net-dbgsym, pd-osc-dbgsym, pd-slip-dbgsym, pd-xbee-dbgsym" >> debian/.debhelper/pd-mrpeach/dbgsym-root/DEBIAN/control\
	;fi

DEB_COPYRIGHT_CHECK_IGNORE_REGEX = \
        ^\./\.git/.*|\./debian/|\./midifile/I_Wanna_Be_Sedated\.mid$

licensecheck:
	LANG=C licensecheck -i "$(DEB_COPYRIGHT_CHECK_IGNORE_REGEX)" --deb-machine -r . \
		> debian/copyright_newhints
	cmp debian/copyright_hints debian/copyright_newhints \
		&& rm debian/copyright_newhints
